[![pipeline status](https://gitlab.com/preusx/django-translations-management/badges/master/pipeline.svg)](https://gitlab.com/preusx/django-translations-management/commits/master)
[![coverage report](https://gitlab.com/preusx/django-translations-management/badges/master/coverage.svg)](https://gitlab.com/preusx/django-translations-management/commits/master)

# Django translations management

## Installation

Installing from gitlab:

```bash
pip install git+https://gitlab.com/preusx/django-translations-management.git@1.0.0
```

See the [documentation here](https://preusx.gitlab.io/django-translations-management/).
