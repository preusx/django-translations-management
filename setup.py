import os
from setuptools import setup, find_packages


pkj_name = 'dtm'


setup(
    name='dtm',
    version='1.1.0',
    url='https://gitlab.com/preusx/django-translations-management',
    install_requires=['django>=1.9', 'django-sendfile2==0.7.1', 'polib==1.1.0'],
    tests_require=['django-nose', 'coverage'],
    test_suite="runtests.runtests",
    long_description=open('README.md', 'r').read(),
    license="MIT",
    author="Alex Tkachenko",
    author_email="preusx@gmail.com",
    packages=find_packages(exclude=['runtests']),
    include_package_data=True,
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'Programming Language :: Python',
        'Programming Language :: Python :: 3'
    ]
)
