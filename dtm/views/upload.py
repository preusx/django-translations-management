import os
from contextlib import suppress

from django.http import Http404
from django.core.validators import FileExtensionValidator
from django import forms
from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _

from django_sendfile import sendfile

from dtm.utils import only_superuser, reverse
from dtm.services import inserter
from .utils import FilePathForm, handle_form


class UploadForm(FilePathForm):
    file = forms.FileField(
        required=True,
        validators=[FileExtensionValidator(['po'])]
    )


@only_superuser
def upload(request):
    form = UploadForm(request.POST, files=request.FILES)

    with suppress(forms.ValidationError):
        with handle_form(
            request, form, success_message=_('File upload successful.')
        ):
            inserter.insert(
                path=form.cleaned_data['path'],
                file=form.cleaned_data['file']
            )

    return redirect(reverse('dtm:list'))
