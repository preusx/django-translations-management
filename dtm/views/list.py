from django.template.response import SimpleTemplateResponse, TemplateResponse

from dtm.services import locator
from dtm.utils import only_superuser
from .utils import get_available_languages


def prepare_file_item(file):
    return {
        'path': file,
        'name': '/'.join(file.split('/')[-5:])
    }


def prepare_objects(languages, files):
    return [
        {
            'code': language,
            'name': languages[language],
            'files': list(map(prepare_file_item, files[language]))
        }
        for language in languages
        if language in files
    ]


@only_superuser
def translations_list(request):
    languages = get_available_languages()
    objects = {
        'project': prepare_objects(
            languages, locator.locate(languages=languages, is_project=True)
        ),
        'django': prepare_objects(
            languages, locator.locate(
                languages=languages, paths=[], is_django=True
            )
        ),
        'third_party': prepare_objects(
            languages, locator.locate(
                languages=languages, paths=[], is_third_party=True
            )
        )
    }

    return TemplateResponse(request, [
        'dtm/admin/list_translations.html'
    ], {
        'objects': objects
    })
