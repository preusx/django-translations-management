import os
from itertools import chain
from functools import reduce
from typing import *

import django
from django.apps import apps
from django.conf import settings


def get_parts():
    if settings.SETTINGS_MODULE:
        return settings.SETTINGS_MODULE.split('.')
    else:
        # if settings.SETTINGS_MODULE is None, we are probably in "test" mode
        # and override_settings() was used 
        # see: https://code.djangoproject.com/ticket/25911
        return os.environ.get(ENVIRONMENT_VARIABLE).split('.')


def get_paths(base):
    relatives = (
        ('locale',),
        ('..', 'locale')
    )

    return filter(os.path.exists, (
        os.path.abspath(os.path.join(base, *relative))
        for relative in relatives
    ))


def get_project_paths(project):
    return get_paths(os.path.dirname(project.__file__))


def get_django_paths():
    path = os.path.abspath(os.path.dirname(django.__file__))

    return (
        os.path.join(root, 'locale')
        for root, dirnames, filename in os.walk(path)
        if 'locale' in dirnames
    )


def check_paths(paths):
    return filter(os.path.isdir, paths)


def get_locale_paths(
    locale_paths,
    is_project: bool=True,
    is_django: bool=False,
    is_third_party: bool=False,
    excluded_paths: Union[list, tuple, set]=(),
    excluded_apps: Union[list, tuple, set]=None
):
    paths = [locale_paths]
    parts = get_parts()
    project = __import__(parts[0], {}, {}, [])
    abs_project_path = os.path.normpath(os.path.abspath(os.path.dirname(project.__file__)))

    if is_project:
        paths.append(get_project_paths(project))

    if is_django:
        paths.append(get_django_paths())

    for app_ in apps.get_app_configs():
        if excluded_apps and app_.name in excluded_apps:
            continue

        app_path = app_.path

        if 'contrib' in app_path and 'django' in app_path and not is_django:
            continue

        if not is_third_party and abs_project_path not in app_path:
            continue

        if not is_project and abs_project_path in app_path:
            continue

        paths.append(get_paths(app_path))

    return check_paths(
        path for path in chain(*paths)
        if path not in excluded_paths
    )


def get_language_transformations(language):
    langs = [language]

    if u'-' in language:
        _l, _c = map(lambda x: x.lower(), language.split(u'-', 1))
        langs += [u'%s_%s' % (_l, _c), u'%s_%s' % (_l, _c.upper()), u'%s_%s' % (_l, _c.capitalize())]
    elif u'_' in language:
        _l, _c = map(lambda x: x.lower(), language.split(u'_', 1))
        langs += [u'%s-%s' % (_l, _c), u'%s-%s' % (_l, _c.upper()), u'%s_%s' % (_l, _c.capitalize())]

    return langs


def get_translations(path, languages, pofile_names=()):
    files = (
        os.path.abspath(os.path.join(path, lang_, 'LC_MESSAGES', fn))
        for lang_ in languages
        for fn in pofile_names
    )

    return filter(os.path.isfile, files)


def get_po_files(
    lang,
    paths: Iterable,
    pofile_names=('django.po',)
):
    """
    Scans a couple possible repositories of gettext catalogs for the given
    language code.
    """

    langs = get_language_transformations(lang)
    paths = list(set(map(os.path.normpath, paths)))
    result = set(chain(*(
        get_translations(path, langs, pofile_names=pofile_names)
        for path in paths
    )))

    return sorted(result)


def locate(
    languages=None,
    paths=None,
    is_project: bool=False,
    is_django: bool=False,
    is_third_party: bool=False
):
    if languages is None:
        languages = (code for code, name in settings.LANGUAGES)

    if paths is None:
        paths = settings.LOCALE_PATHS

    paths = list(get_locale_paths(
        paths, is_project=is_project, is_django=is_django,
        is_third_party=is_third_party
    ))
    result = {}

    for code in languages:
        files = get_po_files(code, paths=paths)

        if len(files) > 0:
            result[code] = files

    return result
